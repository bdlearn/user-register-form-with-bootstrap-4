<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>User Registration From</title>

	<!-- Bootstrap 4 CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- my css -->
	<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-4 offset-md-4  from-div">
			<form action="" method="">
				<h3 class="text-center text-info ">User Register</h3>

				<div class="form-group">
					<label for="username">UserName :</label>
					<input type="text" name="" class="form-control form-control-lg">
				</div>
				<div class="form-group">
					<label for="email">Email :</label>
					<input type="email" name="" class="form-control form-control-lg">
				</div>
				<div class="form-group">
					<label for="password">Password :</label>
					<input type="password" name="" class="form-control form-control-lg" >
				</div>
				<div class="form-group">
					<label for="confirm cassword">Confirm Password :</label>
					<input type="password" name="" class="form-control form-control-lg">
				</div>
				<div class="form-group">
					<button  type="submit" class="btn btn-primary btn-block btn-lg">Sing Up</button>
				</div>
				<p class="text-center">Already a member ?<a href="#">Sing In</a></p>
				
			</form>

		</div>	
	</div>
	
</div>


</body>
</html>